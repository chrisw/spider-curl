import pycurl

from io import BytesIO, StringIO
from lxml import etree

OK = 200

class HTTPException(Exception):
    def __init__(self, url, status_code):
        Exception.__init__(self, 'Failure: HTTP Code {}:{}'.format(status_code, url))

class Response(object):
    def __init__(self, c, doc, raw):
        self.url = c.getinfo(pycurl.EFFECTIVE_URL)
        self.doc = doc
        self.raw = raw

    def xpath(self, *args):
        return self.doc.xpath(*args)

    def text(self):
        return raw

def get(url):
    b = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, b)
    try:
        c.perform()
    except:
        raise

    status_code = c.getinfo(pycurl.HTTP_CODE)
    if status_code != OK:
        raise HTTPException(url, status_code)

    raw = b.getvalue().decode('utf-8')
    parser = etree.HTMLParser()
    doc = etree.parse(StringIO(raw), parser)
    return Response(c, doc, raw)
