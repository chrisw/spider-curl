from . import response

class Rule(object):
    def __init__(self, xpath='', xpath_recur=False, parser=None, follow=False):
        self.xpath = xpath
        self.xpath_recur = xpath_recur
        self.parser = parser
        self.follow = follow

    def crawlers(self, spider):
        sel = '//' if self.xpath_recur else '/'
        links = spider.res.xpath('{}{}a'.format(self.xpath, sel))

        kwargs = {
                    'rules': spider.rules,
                    'parser': self.parser,
                    'follow': self.follow
                }

        crawlers = []
        for url in self.urls(spider.res, links):
            if url.startswith('/'):
                url = spider.domain + url
            try:
                crawler = Spider(spider.domain, url, **kwargs)
            except:
                raise
            crawlers.append(crawler)
        return crawlers

    def urls(self, res, links):
        urls = []
        for link in links:
            urls.append(link.get('href'))
        return urls


class Spider(object):
    def __init__(self, domain, url, rules=[], parser=None, follow=False):
        self.domain = domain
        self.rules = rules
        self.parser = parser
        self.follow = follow

        try:
            self.res = response.get(url)
        except response.HTTPException as e:
            raise

    def crawl(self):
        items = []
        if self.follow:
            spiders = []
            for rule in self.rules:
                try:
                    spiders += rule.crawlers(self)
                except:
                    raise

            for spider in spiders:
                try:
                    for item in spider.crawl():
                        items = self.reducer(items, item)
                except:
                    raise

        if self.parser:
            i = self.parser(self.res)
            i = i if isinstance(i, list) else [i]
            for item in i:
                items = self.reducer(items, item)
        return items

    def reducer(self, items, item):
        items.append(item)
        return items
