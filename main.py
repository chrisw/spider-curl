import sys
import re
import json
import zipcode

import urllib.parse as urlp

from csv import DictWriter
from src.spider import Spider, Rule

keys = ['AT Car ID', 'VIN', 'Condition', 'Make', 'Model', 'Trim', 'Year',
        'Exterior Color', 'Interior Color', 'Transmission', 'Drive Type',
        'Price', 'Mileage', 'Location', 'Comments', 'Detail Page', 'Carfax',
        'Thumbnail']


def safe(xpath, pos, default):
    if len(xpath) > pos:
        return xpath[pos]
    return default


def parse_car(res):
    name = res.xpath(
            '//div[contains(@class, "atcui-column")]/h2/span[@title]/text()')[0].strip()
    name = name.split(' ')
    colors = res.xpath('//span[contains(@class,"colorName")]/span/text()')
    mileage = res.xpath('//span[@class="mileage"]/text()')
    location = (res.xpath('//span[@class="cityStateZip"]/text()')[0].strip() or
                res.xpath('//span[@class="cityStateZip"]/span/text()')[0].strip())
    item = {}
    item['AT Car ID'] = safe(res.xpath('//li[contains(@class, "atCarID")]/text()'), 0, 'Unavailable').strip()
    item['VIN'] = safe(res.xpath('//span[contains(@id, "vinInfoBlock")]/text()'), 0, 'Unavailable').strip()
    item['Condition'] = name[0]
    item['Make'] = name[2]
    item['Model'] = ' '.join(name[3:])
    item['Trim'] = safe(res.xpath('//span[@class="heading-trim"]/text()'), 0, 'Unavailable').strip()
    item['Year'] = name[1]
    item['Exterior Color'] = colors[0]
    item['Interior Color'] = colors[1]
    item['Transmission'] = colors[2]
    item['Drive Type'] = colors[3]
    item['Price'] = safe(res.xpath('//span[contains(@title, "Car Price")]/text()'), 0, 'Unavaiable')
    item['Mileage'] = mileage[0].split(' ')[1] if len(mileage) > 0 else 0
    item['Location'] = location
    item['Comments'] = safe(res.xpath('//div[@class="overview-comments"]/p/text()'), 0, 'None').strip()
    item['Detail Page'] = res.url
    item['Carfax'] = safe(res.xpath('//a[contains(@data-birf-extra, "CARFAX")]/@href'), 0, 'Unavailable')
    return item


def parse_page(res):
    domain = 'http://www.autotrader.com'
    items = []
    for element in res.xpath('//div[contains(@class, "listing-findcar")]'):
        item = {}
        item['Detail Page'] = \
            domain + element.xpath('./a/@href')[0]
        item['Thumbnail'] = \
            safe(element.xpath('.//img[contains(@class, "atcui-vehicle-image")]/@data-original'), 0, 'Unavailable')
        items.append(item)
    return items


class ATPaginationRule(Rule):
    def __init__(self, xpath='', xpath_recur=False, parser=None, follow=False):
        Rule.__init__(self, xpath, xpath_recur, parser, follow)

    def urls(self, res, links):
        urls = []
        for link in links:
            if link.get('data-qaid') == 'lnk-pagNxt':
                split = list(urlp.urlsplit(res.url))
                qs = dict(urlp.parse_qsl(split[3]))
                offset = int(qs['firstRecord']) if 'firstRecord' in qs else 0
                qs['firstRecord'] = offset + 25
                split[3] = urlp.urlencode(qs)
                urls.append(urlp.urlunsplit(split))
        return urls


class ATCarRule(Rule):
    def __init__(self, xpath='', xpath_recur=False, parser=None, follow=False):
        Rule.__init__(self, xpath, xpath_recur, parser, follow)

    def urls(self, res, links):
        urls = []
        for link in links:
            href = link.get('href')
            if re.search('vehicledetails', href):
                urls.append(href)
        return urls


class AutoTraderSpider(Spider):
    base_url = 'http://www.autotrader.com/cars-for-sale'
    domain = 'http://www.autotrader.com'
    rules = [ATCarRule(xpath='//div[contains(@class, "listing-findcar")]',
                       parser=parse_car),
             ATPaginationRule(xpath='//form[contains(@id, "listingsPagination")]',
                              parser=parse_page, follow=True)]

    def __init__(self, make=None, model=None, zip_code=None, radius=25):
        url = self.gen_url(make, model, zip_code, radius)
        Spider.__init__(self, self.domain, url, parser=parse_page, rules=self.rules, follow=True)

    def reducer(self, items, item):
        for i in range(len(items)):
            if items[i]['Detail Page'] == item['Detail Page']:
                items[i].update(item)
                return items
        items.append(item)
        return items

    def location(self, info):
        return '{}+{}-{}'.format(info['city'], info['state'], info['zip_code'])

    def gen_url(self, make, model, zip_code, radius):
        info = zipcode.info(zip_code)
        loc = self.location(info)
        url = self.base_url
        if make:
            url = '{}/{}'.format(url, make)
            if model:
                url = '{}/{}'.format(url, model)
        return '{}/{}?searchRadius={}'.format(url, loc, radius)


if __name__ == '__main__':
    with open(sys.argv[1], 'r') as config_file:
        config = json.load(config_file)
        spider = AutoTraderSpider(**config)
        items = spider.crawl()
        print('Found {} items'.format(len(items)))
    with open(sys.argv[2], 'w') as output_file:
        dw = DictWriter(output_file, keys)
        dw.writeheader()
        dw.writerows(items)
